Vue.component('dice-roller',{
    template: "<div><div>{{ currentRoll }}</div> <button v-on:click='rollDie'>Roll dice</button></div>",
    data: function () {
        return{
            currentRoll: 0
        }
    },
    methods:{
            rollDie: function () {
                this.currentRoll = 1 + Math.floor(Math.random() * 6);

            }
    }
});




//start a new vue app
new Vue({
    el: "#app"
});