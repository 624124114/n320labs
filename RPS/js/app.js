Vue.component("win-meter", {
    template: "<div class='bar' v-bind:style='{ width: shownAmount+\"px\"}'></div>",
    data:function(){
        return{
            shownAmount: 10
        }
    },
    pprops: [ "amount"],
    watch: {
        amount: function(newValue, oldValue) {
            TweenMax.to(this, .7, { shownAmount: newValue});
        }

    }
});

Vue.component("rock-paper-scissors", {
    template: "#rps-template",
    data: function () {
        return{
            computerMove: "nothing",
            userMove:"nothing",
            outcome: "tie"
        }

    },
    methods: {
        choose: function (userMove) {
            this.userMove = userMove;
            let possibleMoves = ["rock", "paper", 'scissors'];
            this.computerMove = possibleMoves[Math.floor(Math.random() * 3)];
            if(this.userMove == "rock") {
                if(this.computerMove == "rock")this.outcome = "tie";
                if(this.computerMove == "paper")this.outcome = "lose";
                if(this.computerMove == "scissors")this.outcome = "win";
            }

            if(this.userMove == "paper"){
                if(this.computerMove == "rock")this.outcome = "win";
                if(this.computerMove == "paper")this.outcome = "tie";
                if(this.computerMove == "scissors")this.outcome = "lose";
            }
            if(this.userMove == "scissors"){
                if(this.computerMove == "rock")this.outcome = "lose";
                if(this.computerMove == "paper")this.outcome = "winn";
                if(this.computerMove == "scissors")this.outcome = "tie";
            }

            if(this.outcome == "win") this.$emit("win");
            if(this.outcome == "lose") this.$emit("lose");

        }
    }
})



new Vue({
    el: "#app",
    data: function () {
        return{
            currentWins: 100
        }

    },
    methods: {
        win: function(){
            this.currentWins ++;

        },
        lose : function(){
            this.currentWins --;
        }
    }
});