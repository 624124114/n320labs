Vue.component("win-meter", {
    template: "<div class='bar' v-bind:style='{ width: shownAmount+\"px\"}'></div>",
    data:function(){
        return{
            shownAmount: 10
        }
    },
    pprops: [ "amount"],
    watch: {
        amount: function(newValue, oldValue) {
            TweenMax.to(this, .7, { shownAmount: newValue});
        }

    }
});