import winmeter from "./WinMeter"
import rps from "./RPS"




new Vue({
    el: "#app",
    data: function () {
        return{
            currentWins: 100
        }

    },
    methods: {
        win: function(){
            this.currentWins ++;

        },
        lose : function(){
            this.currentWins --;
        }
    }
});