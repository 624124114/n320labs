"use strict";
//class is not fully supported
class Tamagotchi {
    //constructs this object - helps to get it set up
    constructor(name, type, energy, selector){
        this.name = name;
        this.type = type;
        this.energy = energy;
         this.element = document.querySelector(selector);

        //start the tamagotchi simulation
        setInterval(this.update.bind(this), 1000);

    }
      //expose some debug data...
    report(){
        console.log(this.name + " is a " + this.type + " and has " + this.energy);
    }
    eat() {
        //add one energys
        this.energy += 10;
    }

    update() {
        this.energy--;
        if (this.energy > 0) {
        this.element.innerHTML = this.name + " has " + this.energy + " energy ";
    } else {this.element.innerHTML = "The tamagotchi has run away"};

    }


}


// make an instance of a tamagotchi
var myTamagotchi = new Tamagotchi("Druid", "heart", 24, "#dvInfo");
myTamagotchi.report();//have the tamagotchi expose some debug data
